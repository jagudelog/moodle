from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium import webdriver
import unittest
import time





class SelectSearch(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"D:\IMPORTANTE\driver\chromedriver.exe")
        
    def test_using_select(self):
        driver = self.driver
        driver.get("https://www.w3schools.com/howto/howto_custom_select.asp")
        time.sleep(2)
        
        select_to  = driver.find_element(By.XPATH,"//*[@id='main']/div[3]/div[1]/select")
        option_to = select_to.find_elements(By.TAG_NAME,"option")
        time.sleep(2)
        
        for option in option_to:
            print(option.get_attribute("value"))
            option.click()
            time.sleep(2)
            
        selected = Select(driver.find_element(By.XPATH,"//*[@id='main']/div[3]/div[1]/select"))
        selected.select_by_value("10")
        time.sleep(2)

# correr la clase 
if __name__ == '__main__':
    unittest.main()