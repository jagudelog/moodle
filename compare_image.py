from selenium import webdriver
import unittest
import time
import cv2


class CompareImage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Edge(
            executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")

    def test_using_opencv(self):
        driver = self.driver
        driver.get("http://google.com")
        driver.save_screenshot('img2.png')
        time.sleep(3)

    def test_compare_image(self):
        img1 = cv2.imread('img1.png')
        img2 = cv2.imread('img2.png')

        # comparando
        difference = cv2.absdiff(img1,img2)
    
        # identifica mas facil los colores grises
        image_gray = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
        contours,_ = cv2.findContours(
        image_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for c in contours:
            # 20 --> escalacion nivel de grises
            if cv2.contourArea(c) >= 20:
                # dimensiones de la imagen asignadas en estas variables
                position_x, position_y, width, high = cv2.boundingRect(c)

                cv2.rectangle(img1, (position_x, position_y),
                              (position_x + width, position_y + high), (0, 0, 255), 2)

        # mostrando imagen
        while (1):
            cv2.imshow('Image_one', img1)
            cv2.imshow('Image_two', img2)
            cv2.imshow('difference_detected', difference)
            keyboard = cv2.waitKey(5) & 0xFF

            if keyboard == 27:
                break
        cv2.destroyAllWindows()


# correr la clase
if __name__ == '__main__':
    unittest.main()
