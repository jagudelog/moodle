from selenium.webdriver.common.by import By
from selenium import webdriver
import unittest
import time

class RadioButton(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
        
    def test_using_radio_button(self):
        driver = self.driver
        driver.get("https://www.w3schools.com/howto/howto_css_custom_checkbox.asp")
        radio_btn = driver.find_element(By.XPATH,"//*[@id='main']/div[3]/div[1]/input[3]")
        time.sleep(3)
        radio_btn = driver.find_element(By.XPATH,"//*[@id='main']/div[3]/div[1]/input[4]")
        radio_btn.click()
        time.sleep(3)
    
    def tearDown(self):
        self.driver.close()
        
        
# correr la clase 
if __name__ == '__main__':
    unittest.main()