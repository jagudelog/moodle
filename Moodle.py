from selenium.webdriver.common.by import By     #By: para seleccionar elementos
from selenium import webdriver                  #webdriver: contiene todos los métodos
from selenium.webdriver.common.keys import Keys #Keys: para dar enter en la automatización
from selenium.webdriver.support.ui import Select


# from selenium.common.exceptions import WebDriverException
import unittest                     #Testeo
import time                         #Tiempo de cada paso
from openpyxl import load_workbook  #Leer archivos de Excel

class IngresoUsuarios(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Users\jagudelog\OneDrive - Choucair Testing\Escritorio\Mis Documentos\Proyectos Choucair\4 - Ingresos Y Retiros\Jenkins\ChromeDriver\chromedriver_win32\chromedriver.exe")
        
    def test_creacion_usuarios(self):
        
        # instanciando el objeto driver
        driver = self.driver
        
        # asignacion de variables
        
        # credenciales
        user = 'admin'
        password = 'Colombia2020+'
        
        # abre navegador chrome
        driver.get("https://operacion.choucairtesting.com/academy/my/")
        
        # maximiza la pantalla
        driver.maximize_window() 
        time.sleep(5)
        
        #tomar el boton de Ingresar y darle clic
        btningresar = driver.find_element(By.CLASS_NAME, "btn-info")
        btningresar.click()
        time.sleep(2)

        login_usuario = driver.find_element(By.ID, "username")
        login_usuario.send_keys(user)
        time.sleep(3)

        login_usuario = driver.find_element(By.ID, "password")
        login_usuario.send_keys(password)
        time.sleep(3)

        btnacceder = driver.find_element(By.CLASS_NAME, "btn-primary")
        btnacceder.click()
        time.sleep(3)

        btnadminsitio = driver.find_element(By.CLASS_NAME, "siteadminlink")
        btnadminsitio.click()
        time.sleep(3)    

        btnusuarios = driver.find_element(By.XPATH, "//*[@id='region-main']/div/ul/li[2]/a")
        btnusuarios.click()
        time.sleep(3)    

        btncrearusuario = driver.find_element(By.XPATH, "//*[@id='linkusers']/div/div[2]/div[2]/ul/li[3]/a")
        btncrearusuario.click()
        time.sleep(3)  


        # Acá se le daría lectura al excel y hacer el ciclo de asignación.
        filesheet = "C:\\Users\\jagudelog\\OneDrive - Choucair Testing\\Escritorio\\Mis Documentos\\Proyectos Choucair\\6 - Moodle\\Automatizacion_Usuarios_Moodle.xlsx"
        leerxlsx = load_workbook(filesheet)
        leerhoja = leerxlsx.get_sheet_names()
        leercelda = leerxlsx.get_sheet_by_name (leerhoja[0])
        leerxlsx.close()


        for i in range(2,leercelda.max_row+1):
            usuario,contrasena,nombre,apellido,correo,ciudad,pais = leercelda[f'A{i}:G{i}'][0]     

            # Tomar los datos del campo Nombre Usuario
            strusuario = driver.find_element(By.ID,'id_username')
            strusuario.send_keys(usuario.value)
            time.sleep(2)


            # Select para País
            select_to  = driver.find_element(By.ID,"id_auth")
            select_to.click()
            option_to = select_to.find_elements(By.TAG_NAME,"option")
            time.sleep(2)
            selected = Select(driver.find_element(By.ID,"id_auth"))
            selected.select_by_value("email")
            time.sleep(2)
            

            # Da clic en el campo para la Contraseña
            strcliccontrasena = driver.find_element(By.XPATH,'/html/body/div[2]/div[2]/div/div/div/section/div/form/fieldset[1]/div/div[6]/div[2]/span/a[1]/span/span/em')
            strcliccontrasena.click()
            time.sleep(2)

            # Tomar los datos del campo Contraseña
            strcontrasena = driver.find_element(By.ID,'id_newpassword')
            strcontrasena.send_keys(contrasena.value)
            time.sleep(2)

            strnombre = driver.find_element(By.ID,'id_firstname')
            strnombre.send_keys(nombre.value)
            time.sleep(2)

            strapellido = driver.find_element(By.ID,'id_lastname')
            strapellido.send_keys(apellido.value)
            time.sleep(2)

            strcorreo = driver.find_element(By.ID,'id_email')
            strcorreo.send_keys(correo.value)
            time.sleep(2)

            strciudad = driver.find_element(By.ID,'id_city')
            strciudad.send_keys(ciudad.value)
            time.sleep(2)

            if pais.value == "Colombia":
                # Select para País
                select_to  = driver.find_element(By.ID,"id_country")
                select_to.click()
                option_to = select_to.find_elements(By.TAG_NAME,"option")
                time.sleep(2)
                selected = Select(driver.find_element(By.ID,"id_country"))
                selected.select_by_value("CO")
                time.sleep(2)

            elif pais.value == "Ecuador":
                # Select para País
                select_to  = driver.find_element(By.ID,"id_country")
                select_to.click()
                option_to = select_to.find_elements(By.TAG_NAME,"option")
                time.sleep(2)
                selected = Select(driver.find_element(By.ID,"id_country"))
                selected.select_by_value("EC")
                time.sleep(2)

            elif pais.value == "Peru":
                # Select para País
                select_to  = driver.find_element(By.ID,"id_country")
                select_to.click()
                option_to = select_to.find_elements(By.TAG_NAME,"option")
                time.sleep(2)
                selected = Select(driver.find_element(By.ID,"id_country"))
                selected.select_by_value("PE")
                time.sleep(2)

            elif pais.value == "Panama":
                # Select para País
                select_to  = driver.find_element(By.ID,"id_country")
                select_to.click()
                option_to = select_to.find_elements(By.TAG_NAME,"option")
                time.sleep(2)
                selected = Select(driver.find_element(By.ID,"id_country"))
                selected.select_by_value("PA")
                time.sleep(2)

            elif pais.value == "Estados Unidos":
                # Select para País
                select_to  = driver.find_element(By.ID,"id_country")
                select_to.click()
                option_to = select_to.find_elements(By.TAG_NAME,"option")
                time.sleep(2)
                selected = Select(driver.find_element(By.ID,"id_country"))
                selected.select_by_value("US")
                time.sleep(2)

            # Botón de Guardar
            btnguardar = driver.find_element(By.ID,'id_submitbutton')
            btnguardar.click()
            time.sleep(5)


            # Botón de Guardar
            btncrearusuario = driver.find_element(By.XPATH,'/html/body/div[1]/div[2]/div/div/div/section/div/div[2]/form/button')
            btncrearusuario.click()
            time.sleep(5)


    def tearDown(self):
        self.driver.close()


# correr la clase 
if __name__ == '__main__':
    unittest.main()


