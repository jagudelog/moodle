from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
import unittest
import time


# el xpath es la estructura de direcciones 
# existe 2 xpath el absoluto y el relativo
# relativo --> busqueda de una ruta relativa a un nodo que le demos (una parte de la ruta)
class XpathTest(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
        
    def test_using_for_xpath(self):
        driver = self.driver
        driver.get("http://google.com")
        time.sleep(2)
        
        search_for_xpath = driver.find_element(By.XPATH,"/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input[@name='q']")
        # search_for_xpath = driver.find_element("name","q")        
        time.sleep(2)
        
        # le da a la tecla hacia abajo para buscar ese resultado 
        search_for_xpath.send_keys("selenium", Keys.ARROW_DOWN)
        
        time.sleep(3)
        search_for_xpath.send_keys(Keys.RETURN)
        time.sleep(4)
        
    def tearDown(self):
        self.driver.close()        

# correr la clase 
if __name__ == '__main__':
    unittest.main()